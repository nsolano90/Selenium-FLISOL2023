import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


# Definir un fixture que crea una nueva instancia de Selenium Web Driver
@pytest.fixture
def driver():
    # Crea una nueva instancia de chrome driver
    chrome = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))

    # Se cierra una vez que las pruebas se completaron
    yield chrome
    chrome.quit()


# Define una funcion para la prueba a ser ejecutada
def test_titulo(driver):
    # Obtiene la pagina web
    driver.get("https://www.flisol.ec/")

    # Verifica que el titulo es el correcto
    titulo = (
        "FLISOL - El evento de difusión de Software Libre más grande en América Latina"
    )
    assert driver.title == titulo


def test_imagen(driver):
    # Obtiene la pagina web
    driver.get("https://www.flisol.ec/")

    # Verifica que cargue el banner
    assert driver.find_element(By.XPATH, '//*[@id="top"]/div[1]/div[1]/div/a/img')

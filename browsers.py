# Pruebas de Selenium en varios navegadores

import os

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService


# Manipulación de Google Chrome
chrome = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
chrome.get("https://www.flisol.ec/")
chrome.implicitly_wait(10)
chrome.quit()


# Manipulación de Firefox
# En sistemas que usan SNAP hay un bug que aún no se soluciona y se debe de ingresar manualente
# la locación del driver y los binarios de Firefox
try:
    firefox = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))
except Exception as ex:
    print(ex)
    folder_firefox = "/snap/firefox/current/usr/lib/firefox"
    driver_firefox = os.path.join(folder_firefox, "geckodriver")
    binario_firefox = os.path.join(folder_firefox, "firefox")
    service = FirefoxService(driver_firefox)
    opts = webdriver.FirefoxOptions()
    opts.binary_location = binario_firefox
    firefox = webdriver.Firefox(service=service, options=opts)

firefox.get("https://www.flisol.ec/")
firefox.implicitly_wait(10)
firefox.quit()

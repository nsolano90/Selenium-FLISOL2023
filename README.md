# Demo de Selenium para el FLISOl 2023
Selenium es un framework que permite automatización de tareas en el browser.

La presentación se encuentra en el siguiente enlace:

[Presentación de Google](https://docs.google.com/presentation/d/1Z9sQWNuzPZRdayv06YJkmHLzyBksINkxlUFy-bZopfY/edit?usp=sharing)

## Instalación de Python 3.11
Primero se deberá descargar e instalar Python 3.11  en su máquina de acuerdo a su sistema operativo. El enlace de la descarga es el siguiente:

[Python 3.11](https://www.python.org/downloads/release/python-3110/).

## Ambiente virtual de Python 3.11

Posteriormente se deberá instalar el paquete venv para Python 3.11 (Para guía refierase al enlace [venv](https://docs.python.org/3/library/venv.html)). Por ejemplo para distribuciones basadas en Debian:

`sudo apt install python3.11-venv`

Se deberá de crear el ambiente virtual usando venv `python -m venv /path/to/new/virtual/environment`. Por ejemplo dentro de la carpeta en donde se descargaron los componentes anteriores:

`python3.11 -m venv myvenv`

Después se debe de levantar el ambiente virtual usando venv `source /path/to/directory`. Por ejemplo:

`source myvenv/bin/activate`

## Instalación de Selenium

Se usará el gestor de paquetes PIP para instalar el framework SELENIUM en el ambiente virtual de la siguiente manera:

`pip install selenium`

## Instalación de WebDriver Manager

Para la correcta manipulación del navegador se debe de instalar un driver para la web de la siguiente manera:

`pip install webdriver-manager`

Para más información de como manipular los distintos tipos de browsers, referirse a la página de [WebDriver Manager](https://pypi.org/project/webdriver-manager/)

## Instalación de Pytest

Pytest es una lbrería de pruebas muy completa que se usan para pruebas unitarias hy de integración. Se instala de la siguiente manera:

`pip install pytest`

Para ejecutar las pruebas se debe de usar el comando PYTEST de la siguiente manera:

`pytest <archivo_pruebas.py>::<prueba>`







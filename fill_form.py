# Rellenado automatico de un formulario

import os
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager

# Se inicializa el navegador firefox
def open_firefox():
    try:
        firefox = webdriver.Firefox(
            service=FirefoxService(GeckoDriverManager().install())
        )
    except Exception as ex:
        folder_firefox = "/snap/firefox/current/usr/lib/firefox"
        driver_firefox = os.path.join(folder_firefox, "geckodriver")
        binario_firefox = os.path.join(folder_firefox, "firefox")
        service = FirefoxService(driver_firefox)
        opts = webdriver.FirefoxOptions()
        opts.binary_location = binario_firefox
        firefox = webdriver.Firefox(service=service, options=opts)
    return firefox


firefox = open_firefox()
firefox.get("https://openlab.ec/webform/registro_flisol_2023/share")

# Se espera que se cargue el boton submit
WebDriverWait(firefox, 60).until(
    EC.visibility_of_all_elements_located((By.ID, "edit-actions-submit"))
)
# Se ocupan los selectores para llenar los campos del form
nombre = firefox.find_element(By.NAME, "nombres[first]")
nombre.send_keys("Nombre1")
apellido = firefox.find_element(By.ID, "edit-nombres-last")
apellido.send_keys("Apellido1")
correo = firefox.find_element(By.XPATH, '//*[@id="edit-correo-electronico-mail-1"]')
correo.send_keys("napellido1@mimail.com")
correo_conf = firefox.find_element(By.CSS_SELECTOR, "#edit-correo-electronico-mail-2")
correo_conf.send_keys("napellido1@mimail.com")
organizacion = firefox.find_element(By.ID, "edit-organizacion-comunidad")
organizacion.send_keys("Mi organizacion")
sabes = firefox.find_element(By.ID, "edit-sabes-que-es-el-flisol-si-asistente")
sabes.click()
ciudad = firefox.find_element(By.ID, "edit-pais-city")
ciudad.send_keys("GYE")
seleccion = firefox.find_element(By.XPATH, '//*[@id="edit-pais-country"]/option[62]')
seleccion.click()
sexo = firefox.find_element(By.ID, "edit-sexo-otro")
sexo.click()
ayuda = firefox.find_element(
    By.XPATH,
    '//*[@id="edit-quieres-ayuda-para-instalar-software-libre-en-tu-computadora-"]/div[2]/label',
)
ayuda.click()
ocupacion = firefox.find_element(
    By.XPATH, '//*[@id="edit-tu-ocupacion-radios"]/div[3]/label'
)
ocupacion.click()
sede = firefox.find_element(By.XPATH, '//*[@id="edit-sede"]/div[4]/label')
sede.click()
terminos = firefox.find_element(
    By.XPATH, '//*[@id="webform-submission-registro-flisol-2023-add-form"]/div[3]/label'
)
terminos.click()

# Se pulsa la tecla de flecha abajo 
firefox.find_element(By.TAG_NAME, "body").send_keys(Keys.ARROW_DOWN)
time.sleep(5)

# Se envia los datos al servidor
enviar = firefox.find_element(By.ID, "edit-actions-submit")
enviar.click()
time.sleep(5)

# Close the driver
firefox.quit()
